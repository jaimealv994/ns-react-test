const stringToComponent = (value: string | Array<any>, valuesToReplace: Array<string>, mapper: Function) => {
  let result = typeof value === 'string' ? [value] : [...value];

  valuesToReplace.forEach((valueToReplace) => {
    const iter = [];

    result.forEach((section) => {
      if (typeof section === 'string') {
        section.split(valueToReplace).forEach((val, index, array) => {
          iter.push(val);
          if (array.length - 1 !== index) {
            iter.push(mapper(valueToReplace));
          }
        });
      } else {
        iter.push(section);
      }
    });

    result = iter;
  });

  return result;
};


export default stringToComponent;
