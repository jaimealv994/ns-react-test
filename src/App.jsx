import React, { useCallback, useState } from 'react';
import Toolbar from './components/UI/Toolbar';
import ProfileList from './components/Profile/List';
import Button from './components/UI/Button/Button';
import useConfig from './components/Config/useConfig';
import ConfigDialog from './components/Config/Dialog/Dialog';

function App() {
  const { profilesConfig, setProfilesConfig } = useConfig();
  const [isConfigOpen, setIsConfigOpen] = useState(false);

  const toggleDialogVisibility = useCallback(() => {
    setIsConfigOpen(prev => !prev);
  }, []);

  return (
    <>
      <Toolbar>
        <div
          className="width-wrapper flex"
          style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between', margin: '0 auto' }}
        >
          <h1>Twitter Client</h1>
          <Button type="outlined" onClick={toggleDialogVisibility}>Configure</Button>
        </div>
      </Toolbar>
      <ProfileList profilesConfig={profilesConfig} />

      <ConfigDialog
        open={isConfigOpen}
        profilesConfig={profilesConfig}
        onClose={toggleDialogVisibility}
        onSubmit={setProfilesConfig}
      />
    </>
  );
}

export default App;
