import React from 'react';
import TweetLink from '../UI/Link';

type HashTagProps = {
    hashTag: string
}
const TweetHashTag = (props: HashTagProps) => {
  const { hashTag } = props;


  return (
    <TweetLink target={`https://twitter.com/hashtag/${hashTag.replace('#', '')}?src=hash`} text={hashTag}>
      {hashTag}
    </TweetLink>
  );
};

export default TweetHashTag;
