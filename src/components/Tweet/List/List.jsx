import React from 'react';
import TweetListItem from './Item';
import type { Tweet } from '../../../model/tweet';
import List from '../../UI/List';

type Props = {
    tweets: Array<Tweet>
}
const TweetList = (props: Props) => {
  const { tweets } = props;


  return (
    <List>
      {tweets.map((tweet: Tweet) => (
        <TweetListItem key={tweet.id} tweet={tweet} />
      ))}
    </List>
  );
};


export default TweetList;
