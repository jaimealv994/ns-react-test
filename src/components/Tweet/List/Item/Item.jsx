import React from 'react';
import type { Tweet } from '../../../../model/tweet';
import TweetContent from '../../Content';
import styles from './Item.module.scss';
import ListItem from '../../../UI/List/Item/Item';
import TweetAuthorship from '../../Authorship';
import Retweeter from '../../Authorship/Retweeter';

type TweetListItemProps = {
    tweet: Tweet
}
const TweetListItem = (props: TweetListItemProps) => {
  const { tweet } = props;

  return (
    <a
            // eslint-disable-next-line react/jsx-no-target-blank
      target="_blank"
      className={styles.tweetItem}
      href={`https://twitter.com/${tweet.authorName}/status/${tweet.id}`}
    >
      <ListItem>
        {tweet.retweetwer && (<Retweeter retweetwer={tweet.retweetwer} />)}
        <TweetAuthorship author={tweet.author} authorName={tweet.authorName} date={tweet.timeMs} />
        <TweetContent text={tweet.text} mentions={tweet.mentions} hashTags={tweet.hashtags} />
      </ListItem>
    </a>
  );
};


export default TweetListItem;
