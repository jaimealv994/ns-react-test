import React, { useMemo } from 'react';
import classNames from 'classnames';
import ProfileLink from '../../Profile/Link';
import styles from './Authorship.module.scss';

type TweetAuthorshipProps = {
    author: string,
    authorName: string,
    date: number
}
const TweetAuthorship = (props: TweetAuthorshipProps) => {
  const { author, authorName, date } = props;

  const formattedDate = useMemo(() => new Date(typeof date === 'string' ? Number(date) : date).toLocaleDateString(), [date]);

  return (
    <div className={styles.container}>
      <ProfileLink username={author}>
        <span className={classNames(styles.authorName, styles.link)}><strong>{authorName}</strong></span>
        <span className={classNames('gutters', styles.link)}>
@
          <b>{author}</b>
        </span>
      </ProfileLink>
      <small>{formattedDate}</small>
    </div>
  );
};

export default TweetAuthorship;
