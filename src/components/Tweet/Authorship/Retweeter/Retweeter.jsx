import React from 'react';
import styles from './Retweeter.module.scss';

type Props = {
    retweetwer: string
}
const Retweeter = (props: Props) => {
  const { retweetwer } = props;

  return (
    <span className={styles.retweetwer}>
      <i>
        {`${retweetwer} retweeted`}
      </i>
    </span>
  );
};

export default Retweeter;
