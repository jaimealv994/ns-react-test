import React, { useMemo } from 'react';
import TweetMention from '../Mention';
import stringToComponent from '../../../utils/stringToComponent';
import TweetHashTag from '../HashTag';

type TweetContentProps = {
    text: string,
    mentions: Array<string>,
    hashTags: Array<string>
}
const TweetContent = (props: TweetContentProps) => {
  const { mentions, text, hashTags } = props;


  const content = useMemo(() => {
    if (!mentions.length) {
      return text;
    }


    return stringToComponent(
      stringToComponent(text, mentions, mention => <TweetMention key={mention} mention={mention} />),
      hashTags, hashTag => <TweetHashTag key={hashTag} hashTag={hashTag} />,
    );
  }, [mentions, text, hashTags]);


  return (
    <div>
      <p>{content}</p>
    </div>
  );
};

export default TweetContent;
