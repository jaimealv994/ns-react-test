import React from 'react';
import TweetLink from '../UI/Link';

type TweetMentionProps = {
    mention: string
}
const TweetMention = (props: TweetMentionProps) => {
  const { mention } = props;
  return (
    <TweetLink target={`https://twitter.com/${mention.replace('@', '')}`}>
      {mention}
    </TweetLink>
  );
};

export default TweetMention;
