import { useEffect, useState } from 'react';
import type { ProfileConfig } from '../../model/profileConfig';
import { getProfileTweets } from '../../service/tweet';

const useProfileTweets = (profileConfig: ProfileConfig) => {
  const [tweets, setTweets] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getProfileTweets(profileConfig).then((result) => {
      setTweets(result.data);
    }).finally(() => setIsLoading(false));
  }, [profileConfig.pagesCount, profileConfig.profile]);

  return { tweets, isLoading };
};

export default useProfileTweets;
