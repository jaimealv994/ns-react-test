import React from 'react';
import TweetLink from '../UI/Link';

type ProfileLinkProps = {
    username: string,
    children: any
}
const ProfileLink = (props: ProfileLinkProps) => {
  const { username, children } = props;


  return (
    <TweetLink target={`https://twitter.com/${username.replace('@', '')}`}>
      {children}
    </TweetLink>
  );
};

export default ProfileLink;
