import React from 'react';
import classNames from 'classnames';
import type { ProfileConfig } from '../../../model/profileConfig';
import ProfileListItem from './Item';
import styles from './List.module.scss';

type Props = {
    profilesConfig: Array<ProfileConfig>
}
const ProfileList = (props: Props) => {
  const { profilesConfig } = props;

  return (
    <div className={classNames(styles.listWrapper, 'width-wrapper')}>
      {profilesConfig.map(profileConfig => (
        <ProfileListItem
          key={profileConfig.profile}
          {...profileConfig}
        />
      ))}
    </div>
  );
};


export default ProfileList;
