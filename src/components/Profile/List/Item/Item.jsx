import React, { memo } from 'react';
import type { ProfileConfig } from '../../../../model/profileConfig';
import useProfileTweets from '../../useProfileTweets';
import LoaderWrapper from '../../../UI/LoaderWrapper';
import TweetList from '../../../Tweet/List';
import styles from './Item.module.scss';

type Props = ProfileConfig
const ProfileListItem = (props: Props) => {
  const { profile, pagesCount } = props;

  const { isLoading, tweets } = useProfileTweets({ profile, pagesCount });

  return (
    <LoaderWrapper isLoading={isLoading}>
      <h2 className={styles.title}>{profile}</h2>
      <TweetList tweets={tweets} />
    </LoaderWrapper>
  );
};

export default memo(ProfileListItem);
