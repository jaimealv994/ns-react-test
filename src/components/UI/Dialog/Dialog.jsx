import React, { useEffect } from 'react';
import styles from './Dialog.module.scss';

type DialogProps = {
    open: boolean,
    children: any
}
const Dialog = (props: DialogProps) => {
  const { open, children } = props;

  useEffect(() => {
    document.querySelector('body').style.overflow = open ? 'hidden' : 'initial';
  }, [open]);

  if (!open) return null;

  return (
    <div className={styles.root}>
      <div className={styles.backdrop} />
      <div className={styles.container}>
        <div className={styles.paper}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default Dialog;
