import React from 'react';
import classNames from 'classnames';
import styles from './Button.module.scss';


type ButtonProps = {
    type: string
}
const Button = (props: ButtonProps) => {
  const { type, ...others } = props;


  return (
    <button
      className={classNames(styles.button, { [styles.outlined]: type === 'outlined' })}
      type="button"
      {...others}
    />
  );
};

export default Button;
