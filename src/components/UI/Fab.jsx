import React from 'react';

type Props = {
    children: any
};
const Fab = (props: Props) => <button type="button" {...props} />;

export default Fab;
