import React from 'react';
import styles from './Link.module.scss';

type TweetLinkProps = {
    children: any,
    target: string,
}

const TweetLink = (props: TweetLinkProps) => {
  const { target, children } = props;

  // eslint-disable-next-line react/jsx-no-target-blank
  return <a className={styles.link} target="_blank" href={target}>{children}</a>;
};

export default TweetLink;
