import React from 'react';

type Props = {
    children: any,
    isLoading: boolean
}
const LoaderWrapper = (props: Props) => {
  const { children, isLoading } = props;

  return (
    <div>
      {children}
      {isLoading && 'Is loading...'}
    </div>
  );
};

export default LoaderWrapper;
