import React from 'react';
import classNames from 'classnames';
import styles from './Toolbar.module.scss';

const Toolbar = props => <div className={classNames('gutters', styles.toolbar)} {...props} />;

export default Toolbar;
