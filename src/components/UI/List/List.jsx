import React from 'react';
import styles from './List.module.scss';

type ListProps = {
    children: any
}
const List = (props: ListProps) => {
  const { children } = props;

  return (
    <ul className={styles.list}>
      {children}
    </ul>
  );
};


export default List;
