import React from 'react';
import styles from './Item.module.scss';

type ListItemProps = {
    children: any
}
const ListItem = (props: ListItemProps) => {
  const { children } = props;
  return (
    <li className={styles.listItem}>
      {children}
    </li>
  );
};


export default ListItem;
