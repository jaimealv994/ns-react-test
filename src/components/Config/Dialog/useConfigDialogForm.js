import { useCallback, useEffect, useState } from 'react';
import type { ProfileConfig } from '../../../model/profileConfig';


const useConfigDialogForm = (profilesConfig: Array<ProfileConfig>) => {
  const [config, setConfig] = useState([]);

  useEffect(() => {
    setConfig(profilesConfig.map(profile => ({ ...profile })));
  }, [profilesConfig]);

  const handleChange = useCallback((event) => {
    const { value, name } = event.target;
    setConfig(prev => prev.map((profileConfig: ProfileConfig) => {
      if (profileConfig.profile === name) {
        return { ...profileConfig, pagesCount: Number(value) };
      }
      return profileConfig;
    }));
  }, []);

  const handleMove = useCallback((event) => {
    const { id, innerText } = event.target;

    setConfig((prev) => {
      const swapped = [...prev];
      const index = prev.map((profileConfig: ProfileConfig) => profileConfig.profile).indexOf(id);
      const isDown = innerText === '↓';
      // eslint-disable-next-line no-restricted-properties
      const nextIndex = index + Math.pow(-1, !isDown);

      const temp = swapped[index];
      swapped[index] = swapped[nextIndex];
      swapped[nextIndex] = temp;

      return swapped;
    });
  }, []);

  return { handleChange, config, handleMove };
};


export default useConfigDialogForm;
