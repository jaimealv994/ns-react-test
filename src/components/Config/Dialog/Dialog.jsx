import React, { useCallback } from 'react';
import Dialog from '../../UI/Dialog/Dialog';
import ConfigForm from '../Form';
import Button from '../../UI/Button/Button';
import styles from './Dialog.module.scss';
import type { ProfileConfig } from '../../../model/profileConfig';
import useConfigDialogForm from './useConfigDialogForm';


type ConfigDialogProps = {
    open: boolean,
    profilesConfig: Array<ProfileConfig>,
    onClose: Function,
    onSubmit: Function
}
const ConfigDialog = (props: ConfigDialogProps) => {
  const {
    open, profilesConfig, onClose, onSubmit,
  } = props;

  const { handleChange, config, handleMove } = useConfigDialogForm(profilesConfig);

  const handleSubmit = useCallback(() => {
    onSubmit(config);
    onClose();
  }, [config]);

  return (
    <Dialog open={open}>
      <h1 className={styles.title}>App Configurations</h1>
      <ConfigForm profilesConfig={config} onChange={handleChange} onMove={handleMove} />
      <div className={styles.actions}>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={handleSubmit}>Save</Button>
      </div>
    </Dialog>
  );
};

export default ConfigDialog;
