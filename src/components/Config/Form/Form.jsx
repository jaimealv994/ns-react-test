import React from 'react';
import type { ProfileConfig } from '../../../model/profileConfig';
import Button from '../../UI/Button/Button';
import styles from './Form.module.scss';

type ConfigFormProps = {
    profilesConfig: Array<ProfileConfig>,
    onChange: Function,
    onMove: Function
}
const ConfigForm = (props: ConfigFormProps) => {
  const { profilesConfig, onChange, onMove } = props;

  return (
    <div>
      {profilesConfig.map((profileConfig: ProfileConfig, index) => (
        <div>
          <Button disabled={index === 0} id={profileConfig.profile} onClick={onMove}>&uarr;</Button>
          <Button
            disabled={index + 1 === profilesConfig.length}
            id={profileConfig.profile}
            onClick={onMove}
          >
                        &darr;
          </Button>
          <strong className={styles.profileName}>{profileConfig.profile}</strong>
          <input
            type="number"
            value={profileConfig.pagesCount}
            name={profileConfig.profile}
            min={0}
            onChange={onChange}
          />
        </div>
      ))}
    </div>
  );
};


export default ConfigForm;
