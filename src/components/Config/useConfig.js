import { useState } from 'react';

const useConfig = () => {
  const [profilesConfig, setProfilesConfig] = useState([
    { profile: 'VersaAgency', pagesCount: 1 },
    { profile: 'RainAgency', pagesCount: 1 },
    { profile: 'alexadevs', pagesCount: 1 }]);


  return { profilesConfig, setProfilesConfig };
};

export default useConfig;
