export type ProfileConfig = {
    profile: string,
    pagesCount: number
}
