import axios from 'axios';
import type { ProfileConfig } from '../model/profileConfig';


export const getProfileTweets = (profileConfig: ProfileConfig) => axios.post('http://localhost:5000/getUserTweets', {
  username: profileConfig.profile,
  quantity: profileConfig.pagesCount,
});
