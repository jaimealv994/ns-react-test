# ns-react-test

NicaSource react test

This project was created with create-react-app and uses react-scripts for running.
 
In order to run this application you have to install its dependencies `npm install` and execute `npm start`
Finally this command will launch the app on port 3000.

You also need to run `tweet-webscrapper-api` which is located at https://github.com/jaimealv994/tweet-scrapper-api